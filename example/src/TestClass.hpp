#include <iostream>
#include <string>

using namespace std;

namespace testnamespace {

/// TestClass - a class for testing doxygen syntax
/// 
///  Column A | Column B | Last
///  -------- | -------- | ----
///   2.3     |  4.3     |  3
///   test    |  NO      |  42
///
/// Some text with :math:`\lambda` and \f$\Lambda\f$
///
/// Python code
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~{.py}
/// print('Hello world')
/// a = 5
/// print('NO')
/// tc = TestClass()
/// tc.say_hi(100)
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~
///
/// Some more text
///
///     Indented 4 extra spaces
///     Should be formated like a code example
///     The name of TestClass
///
class TestClass {

public:

/// An empty constructor
TestClass() {};


/// Say hi a number of times -- text after ndash --- mdash
///
/// Example from doxygen docs
///
///   \f[
///    |I_2|=\left| \int_{0}^T \psi(t) 
///             \left\{ 
///                u(a,t)-
///                \int_{\gamma(t)}^a 
///                \frac{d\theta}{k(\theta,t)}
///                \int_{a}^\theta c(\xi)u_t(\xi,t)\,d\xi
///             \right\} dt
///          \right|
///   \f]
void say_hi(int how_many_times=4) {
    /// The welcoming message
    string message = "Hi!";
    for (int i = 0; i < how_many_times; i++)
        cout << message << endl;
}

};

/// A function!
///
/// @param N
///     The number of times to
///     say hi!
void hisayer(int N=2) {
    auto tc = testnamespace::TestClass();
    tc.say_hi(N);
}

} // end namespace testnamespace

