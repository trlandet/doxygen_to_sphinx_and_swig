Doxygen XML to Sphinx ReStructuredText + SWIG docstrings.i
==========================================================

This script is intended to replace (parts of) Sphinx extension
``breathe`` and the ``doxy2swig`` script.

Autodoc-like constructs in Sphinx is not supported, you need
to loop over the objects in your C++ namespace and output them
to RST yourself (very easy). This makes Sphinx **very** fast on
largish C++ projects, breathe can be rather slooow.

The SWIG generation is pretty much feature complete and also
much faster than doxy2swig, probably mostly because of the use
of minidom by the doxy2swig.py file. Pythons old minidom module
is not a speed deamon ...

Only tested on doxygen XML output from FEniCS dolfin C++ code. On
this code base it takes less than 1 second, while doxy2swig takes
more than 30 seconds and breathe takes 2-3 minutes.

There is a small test case included::

	cd example
	doxygen
	python ../parse_doxygen.py doxygen/xml testnamespace
	
The example ``__main__`` in ``parse_doxygen.py`` produces files

* ``api.rst``
* ``docstrings.i``

You will probably need to write your own script that imports
``parse_doxygen.py`` and does something like ``__main__``, but
tailored to your code base.
 
Copyright Tormod Landet 2017 - Apache 2.0 license
